# Seniors for 2015-2016

+ Keegan Shudy
# First and Second Reader Assignment

+ Cambier   - Kapfhammer, Jumadinova
+ Challener - Kapfhammer, Wenskovitch
+ Jordan    - Kapfhammer, Jumadinova
+ King      - Jumadinova, Kapfhammer
+ Ligouri   - Jumadinova, Wenskovitch
+ Page      - Jumadinova, Kapfhammer
+ Sherretts - Kapfhammer, Jumadinova
+ Yarbrough - Jumadinova, Kapfhammer

# First Reader Breakdown

+ Jumadinova  - King, Ligouri, Page, Yarbrough
+ Kapfhammer  - Cambier, Challener, Sherretts
+ Roos        - None, on sabbatical
+ Wenskovitch - None, visiting assistant professor

# Second Reader Breakdown

+ Jumadinova  - Cambier, Jordan, Sherretts
+ Kapfhammer  - King, Page, Yarbrough
+ Roos        - None, on sabbatical
+ Wenskovitch - Challener, Ligouri

